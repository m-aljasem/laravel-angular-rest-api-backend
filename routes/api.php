<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Employees;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('employees', function() {
    // If the Content-Type and Accept headers are set to 'application/json',
    // this will return a JSON structure. This will be cleaned up later.
    return Employees::all();
});

Route::get('employees/{id}', function($id) {
    return Employees::find($id);
});

Route::post('employees', function(Request $request) {

    $Employee = new Employees();
    $Employee->name = $request->name;
    $Employee->phone = $request->phone;
    $Employee->email = $request->email;


    error_log("Log Start");
    error_log($Employee->name);
    error_log($Employee->phone);
    error_log($Employee->email);
    error_log("Log End");
    $Employee->save();
    //return Employees::create($Employee->name);
});

Route::put('employees/{id}', function(Request $request, $id) {
    $Employees = Employees::findOrFail($id);
    $Employees->update($request->all());

    return $Employees;
});

Route::delete('employees/{id}', function($id) {
    Employees::find($id)->delete();

    return 204;
});


